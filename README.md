covid-to-cword
=============

![](logo.png)

Chrome extension that replaces occurrences of 'covid-19' with 'cunt' and other things because profanity makes everything better. For mature audiences only.

[Direct download for Chrome](https://gitlab.com/j.franco/covid-to-cword/-/raw/master/covid-to-cword.crx?inline=false)

Screenshot Gallery
------------------
https://imgur.com/a/L5ommA5


Installation
------------

In Chrome, choose Window > Extensions.  Drag covid-to-cword.crx into the page that appears.

Tip
------------
If you enjoyed the extension please consider leaving me a tip via PayPpal at https://paypal.me/veganjfranco or via Cash App at $jefranco

Credits
------------------
Forked from https://github.com/panicsteve/cloud-to-butt
